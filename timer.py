#!/usr/bin/env python
"""
A small timer with beep to manage the CCAT question times

Args:
    time (int): Available time in minutes
    questions (int): Number of questions in the test

Returns:
    Prints out question number and sounds a beep with each question.

Raises:
    Exception: raised when values are not intgers.

"""

import os
import time
import sys

try:
    t = int(input("Enter the time in minutes: "))
    q = int(input("Enter the number of questions: "))
except Exception as e:
    raise 'Values must be intgers.'

t *= 60

print(f'\nThe time assigned to each question is {t/q} Seconds.\n')

i = 3
while i > 0:
    print(f"Starting in {i}")
    time.sleep(1)
    i -= 1


os.system("play -q -n synth 0.1 sin 880 || echo -e '\a'")
for i in range(q):
    sys.stdout.write('\r\a\nQuestion {i}'.format(i=i + 1))
    sys.stdout.flush()
    time.sleep(t / q)
    os.system("play -q -n synth 0.1 sin 880 || echo -e '\a'")
